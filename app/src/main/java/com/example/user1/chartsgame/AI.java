package com.example.user1.chartsgame;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class AI {
    private int increaseCnt;
    private int decreaseCnt;
    public int stockAmount;
    private int buyAmount;
    private int sellAmount;
    private float buyTotal;
    private float sellTotal;
    public float earn;
    private int pcount = 0;

    public void AI() {
        this.increaseCnt = 0;
        this.decreaseCnt = 0;
        this.stockAmount = 0;
        this.sellTotal = 0;
        this.sellAmount = 0;
        this.buyAmount = 0;
        this.buyTotal = 0;
        this.earn = 0;
        this.pcount = 0;
    }

    public void Trade(int count, stockAPIData.dataObjects data) {

        if (data.LastPr - data.OpenPr > 0) {
            increaseCnt++;
            decreaseCnt = 0;
        } else if (data.LastPr - data.OpenPr < 0) {
            decreaseCnt++;
            increaseCnt = 0;
        }
        if (decreaseCnt == 2) {
            buyAmount++;
            buyTotal += data.LastPr;
            increaseCnt = 0;
            Log.d("earn", "buy" + this.stockAmount);

        } else if (increaseCnt == 2) {
            sellAmount++;
            decreaseCnt = 0;
            sellTotal += data.LastPr;
            Log.d("earn", "sell");
        }
        this.stockAmount = buyAmount - sellAmount;
        earn = (stockAmount * data.LastPr + sellTotal - buyTotal) * 200 - (buyAmount + sellAmount) * 45;


        Log.d("earn", this.earn + "/" + this.decreaseCnt + "/" + this.increaseCnt);

    }
}
