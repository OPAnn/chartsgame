package com.example.user1.chartsgame;

import java.sql.Date;
import java.util.List;

public class stockAPIData {
    public List<dataObjects> KChartTickList;

    public class dataObjects {
        public String kTickTime;
        public float OpenPr;
        public float LastPr;
        public float HighPr;
        public float LowPr;
        public int TotalQty;
        public float AvgPrice;
        public int RtQty;
        public int DealCount;
        public int TotalInnerCount;
        public float EntrustBuyPr;
        public float EntrustSellPr;
    }
}
