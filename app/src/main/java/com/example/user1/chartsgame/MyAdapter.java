package com.example.user1.chartsgame;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {

    private Context mContext;
    public List<Integer> ids;
    public List<String> details;

    public MyAdapter(Context mContext,List<Integer> ids,List<String> details) {
        this.mContext = mContext;
        this.ids = ids;
        this.details = details;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent,
                                         int viewType) {
        View view = LayoutInflater.from(mContext)
                .inflate(R.layout.recycleview_content, parent, false);
        ViewHolder holder = new ViewHolder(view);


        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.txvID.setText(ids.get(position)+"");
        holder.txvDetails.setText(details.get(position));
    }

    @Override
    public int getItemCount() {
        return ids.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        public TextView txvID;
        public TextView txvDetails;

        public ViewHolder(View itemView) {
            super(itemView);
            txvID = itemView.findViewById(R.id.txvID);
            txvDetails = itemView.findViewById(R.id.txvDetails);

        }
    }
}
