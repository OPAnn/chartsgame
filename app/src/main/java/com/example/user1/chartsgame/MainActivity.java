package com.example.user1.chartsgame;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.CandleStickChart;
import com.github.mikephil.charting.components.LimitLine;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.CandleData;
import com.github.mikephil.charting.data.CandleDataSet;
import com.github.mikephil.charting.data.CandleEntry;
import com.google.gson.Gson;


import java.io.IOException;
import java.lang.reflect.Array;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class MainActivity extends AppCompatActivity {
    //public List<Integer> ids = new ArrayList<>();
    // public List<String> details = new ArrayList<>();

    // AI
    public ImageView imgMeWin;
    public ImageView imgAIWin;
    public TextView txvAITotalAmount;
    public TextView txvAITotalGains;
    public TextView txvAmount;
    public TextView txvTotalAmount;
    public TextView txvTotalGains;
    //public RecyclerView myRecyclerView;
    //public MyAdapter myAdapter;
    float lastPrice;
    int amount = 1;
    int buyAmount = 0;
    int sellAmount = 0;
    int totalAmount = 0;
    float buy = 0;
    float buyTotal = 0;
    float sell = 0;
    float sellTotal = 0;
    float totalGains = 0;
    int dealCount = 0;

    int count = 1;
    stockAPIData apiDatas;
    final Handler handler = new Handler();
    Boolean isFinished = false;
    Boolean isChartInit = false;
    Boolean isGetData = false;
    Boolean isFetching = false;

    //Charts
    ArrayList<CandleEntry> entries = new ArrayList<>();
    ArrayList<BarEntry> barEntries = new ArrayList<>();
    List<Integer> colorSet = new ArrayList<>();
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    List<String> xAxisData = new ArrayList<>();
    myDate checkDate;

    AI ai = new AI();

    Timer timer = new Timer(false);
    TimerTask timerTask = new TimerTask() {
        @Override
        public void run() {
            handler.post(new Runnable() {
                @Override
                public void run() {
                    if (count < 301) {
                        drawChart(count, apiDatas);
                        count++;

                        // 計算
                        lastPrice = apiDatas.KChartTickList.get(count - 1).LastPr;
                        // 總張數
                        totalAmount = buyAmount - sellAmount;
                        // 當下所有張數平掉扣掉總成本的損益
                        totalGains = (totalAmount * lastPrice + sellTotal - buyTotal) * 200 - (buyAmount + sellAmount) * 45;

                        txvTotalAmount.setText("持有張數:" + totalAmount);
                        txvTotalGains.setText(totalGains + "");
                        if (totalGains < 0) {
                            txvTotalGains.setTextColor(Color.GREEN);
                        } else if (totalGains == 0) {
                            txvTotalGains.setTextColor(Color.GRAY);
                        } else {
                            txvTotalGains.setTextColor(Color.RED);
                        }


                        // AI
                        txvAITotalAmount.setText("AI持有張數:" + ai.stockAmount);
                        txvAITotalGains.setText(ai.earn + "");
                        if (ai.earn < 0) {
                            txvAITotalGains.setTextColor(Color.GREEN);
                        } else if (ai.earn == 0) {
                            txvAITotalGains.setTextColor(Color.GRAY);
                        } else {
                            txvAITotalGains.setTextColor(Color.RED);
                        }


                        if (totalGains > ai.earn) {
                            imgMeWin.setVisibility(View.VISIBLE);
                            imgAIWin.setVisibility(View.INVISIBLE);
                        } else if (totalGains < ai.earn) {
                            imgAIWin.setVisibility(View.VISIBLE);
                            imgMeWin.setVisibility(View.INVISIBLE);
                        } else {
                            imgMeWin.setVisibility(View.INVISIBLE);
                            imgAIWin.setVisibility(View.INVISIBLE);
                        }
                    }
                }
            });
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // 螢幕恆亮
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_main);
        try {
            getData();
        } catch (IOException e) {
            e.printStackTrace();
        }
        imgAIWin = findViewById(R.id.AIWinImg);
        imgMeWin = findViewById(R.id.meWinImg);
        txvAITotalAmount = findViewById(R.id.txvAITotalAmount);
        txvAITotalGains = findViewById(R.id.txvAITotalGains);
        txvAmount = findViewById(R.id.txvAmount);
        txvTotalAmount = findViewById(R.id.txvTotalAmount);
        txvTotalGains = findViewById(R.id.txvTotalGains);
//        myRecyclerView = findViewById(R.id.myRecycleview);
//        myRecyclerView.setLayoutManager(new LinearLayoutManager(this));
//        myAdapter = new MyAdapter(this, ids, details);
//        myRecyclerView.setAdapter(myAdapter);
    }

    protected myDate randomDate() {
        myDate date = new myDate((int) (Math.floor((Math.random() * 9)) + 2008), (int) (Math.floor((Math.random() * 11)) + 1), (int) (Math.floor((Math.random() * 27)) + 1));
        return date;
    }

    protected void checkHoliday(final myDate date, final OkHttpClient client) {
        isFetching = true;
        Request request = new Request.Builder().url("http://192.168.10.148/CmMannaWCF/IsHoliday?date8=" + date.Year + date.Month + date.Day + "&ck=936DA01F-9ABD-4d9d-80C7-02AF85C822A8").build();
        Call call = client.newCall(request);
        final myDate tempDate = date;
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.d("Test", "CheckHolidayfail");
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String json = response.body().string();
                Log.d("Test", "success" + json);
                if (json.equals("false")) {
                    isGetData = true;
                    checkDate = tempDate;
                } else {
                    isFetching = false;
                }
            }
        });

    }

    protected void getAPIData(myDate date, OkHttpClient client) {
        Request request = new Request.Builder().url("http://192.168.10.148/TickDbData/api/tick/get?time=" + date.Year + "/" + date.Month + "/" + date.Day).build();
        Call call = client.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.d("Test", "getAPIDATAFail" + "/" + checkDate.Year + "/" + checkDate.Month + "/" + checkDate.Day);

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String json = response.body().string();
                Gson gson = new Gson();
                stockAPIData stockDatas = gson.fromJson(json, stockAPIData.class);
                apiDatas = stockDatas;
                timer.scheduleAtFixedRate(timerTask, 1000, 500);
            }
        });
    }

    protected void getData() throws IOException {
        OkHttpClient client = new OkHttpClient();

        while (!isGetData || checkDate == null) {
            if (!isFetching) {
                checkHoliday(randomDate(), client);
            }
        }
        getAPIData(checkDate, client);
    }


    protected void drawChart(int count, stockAPIData apiDatas) {
        CandleStickChart candleStickChart = (CandleStickChart) findViewById(R.id.chart1);
        BarChart barChart = (BarChart) findViewById(R.id.chart2);

        stockAPIData.dataObjects tempObj;
        tempObj = apiDatas.KChartTickList.get(count - 1);
        CandleEntry temp = new CandleEntry(count - 1, tempObj.HighPr, tempObj.LowPr, tempObj.OpenPr, tempObj.LastPr);
        String tempString = tempObj.kTickTime;
        tempString = tempString.replace("T", " ");
        Date date = new Date();

        date = sdf.parse(tempString, new ParsePosition(0));
        Log.d("Date", date + "");
        xAxisData.add(tempString);
        entries.add(temp);

        barEntries.add(new BarEntry(count - 1, tempObj.TotalQty));

        colorSet.add(tempObj.OpenPr < tempObj.LastPr ? Color.rgb(255, 0, 0) : Color.rgb(0, 255, 0));
        if (tempObj.TotalQty > 1000) {
            colorSet.set(colorSet.size() - 1, Color.YELLOW);
        }

        ai.Trade(count, apiDatas.KChartTickList.get(count - 1));
        float min, max;
        if (count < 30) {
            max = 29.5f;
        } else

        {
            candleStickChart.moveViewToX(count + 0.5f);
            barChart.moveViewToX(count + 0.5f);
            max = count - 0.5f;
        }
        if (!isChartInit) {
            LimitLine startLine = new LimitLine(apiDatas.KChartTickList.get(0).OpenPr, "開盤價");
            startLine.setLineColor(Color.YELLOW);
            startLine.setLineWidth(1f);
            candleStickChart.getAxisLeft().addLimitLine(startLine);
            barChart.getAxisLeft().setLabelCount(3);
            isChartInit = true;
        }
        XAxis xAxis = candleStickChart.getXAxis();

        xAxis.setGranularity(1);

        xAxis.setDrawLabels(false);
        if (candleStickChart.getAxisLeft().getAxisMinimum() > entries.get(0).getOpen() && count > 1) {
            xAxis.setAxisLineColor(Color.YELLOW);
            xAxis.setAxisLineWidth(1f);
        } else if (candleStickChart.getAxisLeft().getAxisMaximum() < entries.get(0).getOpen() && count > 1) {
            xAxis.setAxisLineColor(Color.YELLOW);
            xAxis.setPosition(XAxis.XAxisPosition.TOP);
            xAxis.setAxisLineWidth(1f);
        } else {
            xAxis.setTextColor(Color.WHITE);
            xAxis.setAxisLineColor(Color.WHITE);
            xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
            xAxis.setAxisLineWidth(0.5f);
        }
        candleStickChart.setAutoScaleMinMaxEnabled(true);
        barChart.setAutoScaleMinMaxEnabled(true);
        barChart.getXAxis().setTextColor(Color.WHITE);
        barChart.getAxisLeft().setTextColor(Color.WHITE);
        candleStickChart.getAxisLeft().setTextColor(Color.WHITE);
        candleStickChart.setScaleEnabled(false);
        candleStickChart.setBackgroundColor(Color.BLACK);

        barChart.setBackgroundColor(Color.BLACK);
        candleStickChart.setVisibleXRangeMaximum(30);
        barChart.setVisibleXRangeMaximum(30);
        xAxis.setAxisMaximum(max);
        XAxis xAxisBar = barChart.getXAxis();

        xAxisBar.setGranularity(1);
        xAxisBar.setAxisMaximum(max);
        xAxisBar.setPosition(XAxis.XAxisPosition.BOTTOM);
        YAxis yAxisBar = barChart.getAxisLeft();
        yAxisBar.setDrawGridLines(false);

        barChart.getAxisRight().setDrawGridLines(false);
        barChart.getAxisRight().setEnabled(false);
        candleStickChart.getAxisRight().setEnabled(false);
        candleStickChart.getAxisLeft().setMinWidth(40);
        yAxisBar.setMinWidth(40);
        barChart.getLegend().setEnabled(false);
        candleStickChart.getLegend().setEnabled(false);
        barChart.getDescription().setEnabled(false);
        candleStickChart.getDescription().setEnabled(false);


        CandleDataSet dataset = new CandleDataSet(entries, "");
        BarDataSet datasetBar = new BarDataSet(barEntries, "");
        dataset.setValueTextColor(Color.WHITE);
        datasetBar.setValueTextColor(Color.WHITE);
        datasetBar.setColors(colorSet);
        String[] tempArray = new String[xAxisData.size()];
        tempArray = xAxisData.toArray(tempArray);
        datasetBar.setStackLabels(tempArray);
        dataset.setAxisDependency(YAxis.AxisDependency.LEFT);
        dataset.setShadowColor(Color.DKGRAY);
        dataset.setShadowWidth(2f);
        dataset.setDecreasingColor(Color.GREEN);
        dataset.setDecreasingPaintStyle(Paint.Style.FILL);
        dataset.setIncreasingColor(Color.RED);
        dataset.setIncreasingPaintStyle(Paint.Style.FILL);
        dataset.setNeutralColor(Color.BLACK);

        CandleData data = new CandleData(dataset);
        BarData dataBar = new BarData(datasetBar);
        candleStickChart.setData(data);
        barChart.setData(dataBar);
        candleStickChart.invalidate();
        barChart.invalidate();
    }


    public void btnBuyClick(View view) {

        if (amount > 0) {
            // 當下買入成本
            buy = amount * lastPrice;
            // 總成本
            buyTotal += buy;
            // 交易次數
            dealCount++;
            // 總買入張數
            buyAmount += amount;
            CandleEntry temp = entries.get(entries.size() - 1);
            temp.setIcon(getResources().getDrawable(R.drawable.buy));
            entries.set(entries.size() - 1, temp);

//            ids.add(dealCount);
//            details.add("以" + lastPrice + "買入" + amount + "張");
//            myAdapter.notifyDataSetChanged();
        }

    }

    public void btnSellClick(View view) {

        if (amount > 0) {
            // 當下賣出金額
            sell = amount * lastPrice;
            // 總賣出金額
            sellTotal += sell;
            // 交易次數
            dealCount++;
            // 總賣出張數
            sellAmount += amount;
            CandleEntry temp = entries.get(entries.size() - 1);
            temp.setIcon(getResources().getDrawable(R.drawable.sell));
            entries.set(entries.size() - 1, temp);

//            ids.add(dealCount);
//            details.add("以" + lastPrice + "賣出" + amount + "張");
//            myAdapter.notifyDataSetChanged();
        }

    }

    public void btnAdd(View view) {
        amount++;
        txvAmount.setText(amount + "");
    }

    public void btnMinus(View view) {
        if (amount > 1) {
            amount--;
        }
        txvAmount.setText(amount + "");
    }


}

