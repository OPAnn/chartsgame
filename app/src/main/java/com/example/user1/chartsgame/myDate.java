package com.example.user1.chartsgame;

public class myDate {
    public String Month;
    public String Year;
    public String Day;

    public myDate(float year, float month, float day) {
        Year = (int) year + "";
        Month = month < 10f ? "0" + (int) month : (int) month + "";
        Day = day < 10f ? "0" + (int) day : (int) day + "";
    }
}
